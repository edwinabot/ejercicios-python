"""
Add days to any given date
"""
import unittest


class Date:
    thirty_day_months = [9, 4, 7, 11]

    def __init__(self, year, month, day):
        self.year = year
        self.month = month
        self.day = day
        self._leap_years_since_1970 = []
        self._years_since_1970 = []

    @staticmethod
    def is_leap_year(year):
        return year % 4 == 0 and year % 100 != 0 or year % 400 == 0

    def _get_days_since_absolute_date(self):
        """
        Returns the number of days since 1970/01/01
        :return:
        """
        days = self.day
        for year in range(1970, self.year):
            if self.is_leap_year(year):
                days += 366
                self._leap_years_since_1970.append(year)
            else:
                days += 365
                self._years_since_1970.append(year)

        for month in range(1, self.month):
            if month in self.thirty_day_months:
                days += 30
            elif month == 2:
                days += 29 if self.is_leap_year(self.year) else 28
            else:
                days += 31

        return days

    def _set_date_from_number_of_days(self, days):
        days_counter = days
        year = 1970 + len(self._years_since_1970) + len(self._leap_years_since_1970)
        month = 1

        days_counter -= (366 * len(self._leap_years_since_1970)) + (365 * len(self._years_since_1970))

        while days_counter > 28:

            if self.is_leap_year(year) and (days_counter == 29):
                break

            if month in self.thirty_day_months:
                days_counter -= 30
            elif month == 2:
                days_counter -= 29 if self.is_leap_year(year) else 28
            else:
                days_counter -= 31

            month += 1

        self.year = year
        self.month = month
        self.day = days_counter or 1

    def add_days(self, days):
        absolute_days = self._get_days_since_absolute_date() + days
        self._set_date_from_number_of_days(absolute_days)


class DateTest(unittest.TestCase):

    def test_a_simple_date(self):
        date = Date(2017, 10, 1)
        date.add_days(2)
        self.assertEqual(date.year, 2017)
        self.assertEqual(date.month, 10)
        self.assertEqual(date.day, 3)

    def test_a_leap_year_on_february(self):
        date = Date(2016, 2, 28)
        date.add_days(1)
        self.assertEqual(date.year, 2016)
        self.assertEqual(date.month, 2)
        self.assertEqual(date.day, 29)

    def test_add_60_days(self):
        date = Date(2015, 1, 1)
        date.add_days(60)
        self.assertEqual(date.year, 2015)
        self.assertEqual(date.month, 3)
        self.assertEqual(date.day, 2)


if __name__ == '__main__':
    unittest.main()
